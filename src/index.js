import React from 'react';
import ReactDOM from 'react-dom';
import Cars from './views/Cars/Cars';
import store from './store';
import { Provider } from 'react-redux';

import "./assets/scss/main.scss";

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <Cars />
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);
