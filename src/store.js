import { configureStore } from '@reduxjs/toolkit';
import cars from './views/Cars/carsSlice';

export default configureStore({
  reducer: {
    cars,
  },
});
