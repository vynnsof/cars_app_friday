import React from 'react';

import styles from './Spinner.module.scss';
import spinner from '../../assets/imgs/truck.gif'

const Spinner = () => (
  <div className={styles.spinnerCar}>
    <img src={spinner} alt='' />
  </div>
);

export default Spinner;
