import React, { useState } from 'react';
import PropTypes from 'prop-types';

import Alert from '../Alert/Alert';
import styles from './List.module.scss';

import Spinner from '../Spinner';
import InputSearch from '../InputSearch';

const List = ({
  searhPlaceholder,
  items,
  renderItem,
  isListLoading,
  filter,
  refresh,
  error,
  emptyText,
}) => {
  const [searchText, setSearchText] = useState('');

  if (isListLoading) {
    return <Spinner/>;
  }
  if (items?.length === 0) {
    return  (
      <Alert
        text={(
          <>
            {emptyText && <span>{emptyText}</span>}
            <span>Try another one.</span>
          </>
        )}
      />
    );
  }

  if (error) {
    return  (
      <Alert
        text={(
          <>
            <div>Oops! An error occurred.</div>
            <div> Press <button className={styles.refreshButton} onClick={refresh}>here</button> to try again.</div>
          </>
        )}
      />
    );
  }

  if (items) {
    let filteredItems = searchText && filter ? items.filter(item => filter(item, searchText)) : items;
    return (
      <>
        {filter && <InputSearch placeholder={searhPlaceholder} onSearch={setSearchText} />}
        {filteredItems?.length === 0 ? (
          <Alert
            text={emptyText}
          />
        ): (
            <ul className={styles.list}>
              {filteredItems.map((item) => renderItem(item, styles.listItem))}
            </ul>
        )}
      </>
    );
  }

  return null;
};

List.propTypes = {
  searhPlaceholder: PropTypes.string,
  items: PropTypes.array,
  renderItem: PropTypes.func.isRequired,
  isListLoading: PropTypes.bool.isRequired,
  filter: PropTypes.func,
  refresh: PropTypes.func.isRequired,
  error: PropTypes.object,
  emptyText: PropTypes.string,
};

export default List;
