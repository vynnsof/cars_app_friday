import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import styles from './CloseBtn.module.scss';

  const CloseBtn = ({ text, url }) => (
  <div className={styles.selectedItem}>
    <span>{text}</span>
    <Link to={url} className={styles.close}></Link>
  </div>
);

CloseBtn.propTypes = {
  text: PropTypes.string.isRequired,
  url: PropTypes.string.isRequired,
};

export default CloseBtn;
