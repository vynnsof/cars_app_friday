import React from 'react';
import PropTypes from 'prop-types';

import styles from './Alert.module.scss';

const Alert = ({text}) => {
  return (
    <div className={styles.info}>
      <p>{text}</p>
    </div>
  );
}

Alert.propTypes = {
  text: PropTypes.node.isRequired,
}

export default Alert;
