import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import styles from './Header.module.scss';
import logo from '../../assets/imgs/handshake.png'

const Header = ({ title }) => (
  <header className={styles.pageHeader}>
    <Link to="/" className={styles.logo}>
      <img src={logo} className={styles.logoImg} alt=''/>
      <span>{title}</span>
      </Link>
  </header>
);

Header.propTypes = {
  title: PropTypes.string.isRequired,
};

export default Header;
