import React, { useState, useEffect } from 'react';
import useDebounce from '../../hooks/useDebounce'
import PropTypes from 'prop-types';

import styles from './InputSearch.module.scss';

const InputSearch = ({ onSearch, placeholder }) => {
  const [searchText, setSearchText] = useState('');
  const debouncedSearchText = useDebounce(searchText, 500);

  useEffect(() => {
    onSearch(debouncedSearchText);
  }, [onSearch, debouncedSearchText]);

  return (
      <input
        type="text"
        value={searchText}
        className={styles.InputSearch}
        placeholder={placeholder}
        onChange={(e) => setSearchText(e.target.value)}
      />
  );
};

InputSearch.propTypes = {
  placeholder: PropTypes.string,
  onSearch: PropTypes.func.isRequired,
};

export default InputSearch;
