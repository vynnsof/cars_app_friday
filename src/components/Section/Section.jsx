import React from 'react';
import PropTypes from 'prop-types';
import styles from './Section.module.scss';

const Section = ({ className, children, title }) => (
  <div className={`${styles.section} ${className}`}>
    <div className={styles.head}>{title}</div>
    <div className={styles.content}>{children}</div>
  </div>
);

Section.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node.isRequired,
  title: PropTypes.string.isRequired,
};

export default Section;
