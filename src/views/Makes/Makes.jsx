import React, { useEffect, useCallback } from 'react';
import { Route, Link, useLocation } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { getMakes, selectMakes } from '../Cars/carsSlice';

import Section from '../../components/Section'
import List from '../../components/List';
import CloseBtn from '../../components/CloseBtn';
import Models from '../Models/Models';

import { filterItems } from '../Cars/cars.service';

const Makes = () => {
  const dispatch = useDispatch();
  const { pathname } = useLocation();

  const makes = useSelector(selectMakes);
  const areMakesFetching = useSelector(state => state.cars.areMakesFetching);
  const makesError = useSelector(state => state.cars.makesError);

  const match = pathname.match(/models\/([\w\s]*)/);
  const make = match ? match[1] : null;

  const loadMakes = useCallback(() => {
    dispatch(getMakes());
  }, [dispatch]);

  useEffect(() => {
    loadMakes();
  }, [loadMakes]);

  return (
    <>
      <Section title="Makes">
        {
          make
            ? (
              <div className='d-flex flex-wrap selected'>
              <CloseBtn
                url="/"
                text={make}
              />
              </div>
            )
            : (
              <List
                items={makes}
                isListLoading={areMakesFetching}
                getKey={(make) => make}
                renderItem={(make, className) => (
                  <Link key={make} className={className} to={`/models/${make}`}>{make}</Link>
                )}
                emptyText="No found. Try again!"
                searhPlaceholder="Search"
                refresh={loadMakes}
                filter={filterItems}
                error={makesError}
              />
            )
        }
      </Section>
      <Route path="/models/:make">
        <Models/>
      </Route>
    </>
  )
};

export default Makes;