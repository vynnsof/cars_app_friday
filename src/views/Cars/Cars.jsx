import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import Header from '../../components/Header';

import Makes from '../Makes/Makes';
import styles from './Cars.module.scss';

const Cars = () => (
  <Router>
    <Header title="Choose your dream car" />
    <div className={styles.pageBody}>
        <Makes/>
    </div>
  </Router>
);

export default Cars;
