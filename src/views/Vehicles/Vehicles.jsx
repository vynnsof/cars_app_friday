import React, { useEffect, useCallback, useState } from 'react';
import { useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import Section from '../../components/Section';
import List from '../../components/List';
import Vehicle from '../Vehicle/Vehicle';

import { selectVehicles, getVehicles } from '../Cars/carsSlice';
import { filterVehicles } from '../Cars/cars.service';

import styles from './Vehicles.module.scss';

const Vehicles = () => {
  let { make, model } = useParams();
  const dispatch = useDispatch();

  const [selectedVehicle, setSelectedVehicle] = useState(null);
  const vehicles = useSelector(state => selectVehicles(state, make, model));
  const vehiclesFetching = useSelector(state => state.cars.vehiclesFetching);
  const vehiclesError = useSelector(state => state.cars.vehiclesError);

  const loadVehicles = useCallback(() => {
    if (make && model) {
      dispatch(getVehicles({ make, model }));
    }
  }, [dispatch, make, model]);

  useEffect(() => {
    loadVehicles();
  }, [loadVehicles]);

  return (
    <Section className={styles.vehiclesSection} title="Vehicles">
      {selectedVehicle ? (
        <div className={styles.selectedVehicleContainer}>
          <Vehicle isSelected vehicle={selectedVehicle} onSelect={setSelectedVehicle} />
        </div>
      ) : (
        <List
          items={vehicles}
          isListLoading={vehiclesFetching}
          renderItem={(vehicle) => (
            <Vehicle
              key={vehicle.id}
              onSelect={setSelectedVehicle}
              vehicle={vehicle}
            />
          )}
          emptyText="No found."
          searhPlaceholder="Filter"
          refresh={loadVehicles}
          filter={filterVehicles}
          error={vehiclesError}
        />
    
      )}
    </Section>
  )
};

export default Vehicles;
