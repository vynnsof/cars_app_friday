import React from 'react';
import PropTypes from 'prop-types';
import styles from './Vihicle.module.scss';

const Vehicle = ({ vehicle, onSelect, isSelected }) => {
  return (
    <div className={styles.vehicle} onClick={() => onSelect(vehicle)}>
      {isSelected && (
        <button
          onClick={(e) => {
            e.stopPropagation();
            onSelect(null);
          }}
          className={styles.close}
        >
        </button>
      )}
          <span className={styles.bodyType}><i>Body type:</i> {vehicle.bodyType}</span>
          <span className={styles.fuelType}><i>Fuel type:</i> {vehicle.fuelType}</span>
          <span className={styles.engineCapacity}><i>Engine characteristics</i> {vehicle.engineCapacity} cm³ / {vehicle.enginePowerKW} kw / {vehicle.enginePowerPS} ps</span>
    </div>
  )
};

Vehicle.propTypes = {
  vehicle: PropTypes.shape({
    make: PropTypes.string.isRequired,
    model: PropTypes.string.isRequired,
    bodyType: PropTypes.string.isRequired,
    fuelType: PropTypes.string.isRequired,
    engineCapacity: PropTypes.number.isRequired,
    enginePowerKW: PropTypes.number.isRequired,
    enginePowerPS: PropTypes.number.isRequired,
  }).isRequired,
};

export default Vehicle;
